
public class Microwave
{
	private int watts;
	private String brand;
	private int size;

	public Microwave(int watts, String brand, int size)
	{
		this.watts = watts;
		this.brand = brand;
		this.size = size;
	}

	public int getWatts()
	{
		return watts;
	}
	public void setWatts(int watts)
	{
		this.watts = watts;
	}
	
	public String getBrand()
	{
		return brand;
	}
	public void setBrand(String brand)
	{
		this.brand = brand;
	}

	public int getSize()
	{
		return size;
	}
	public void setSize(int size)
	{
		this.size = size;
	}

	public void printDetails()
	{
		System.out.println("This microwave has " + this.watts + " watts and the size is " + this.size + " and the microwave is from the brand " + this.brand);
	}
	public void reheat(int seconds)
	{
		for (int i = seconds;i >= 0; i--)
		{
			System.out.println("Item will be heated in " + i);
		}
	}

	private boolean verifyPos(int num)
	{
		if (num > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public void changeWattage(int watts)
	{
		boolean tOF = verifyPos(watts);
		if(tOF == true)
		{
			System.out.println("Changing wattage from " + this.watts + " to " + watts);
			this.watts = watts;
		}
		else
		{
			System.out.println("Wattage is not posivite or equal to 0 will not be changing ");
		}
		
	}
}